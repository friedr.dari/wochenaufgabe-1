class Wuerfeltester {
    public static void main(String[] args) {
        Wuerfel w = new Wuerfel(6, 6);
        boolean isFair = test(w);
        System.out.println("Der Würfel ist fair: " + isFair);
    }

    /**
     * Testet, ob ein Würfel "fair" (siehe README) ist.
     *
     * @param w Der zu testende Würfel
     * @return true, wenn der Würfel fair ist, sonst false
     */
    public static boolean test(Wuerfel w) {
        // Die Seitenzahl ist um 1 größer als die Differenz der Werte
        int[] observedCounts = new int[w.getMax() - w.getMin() + 1];

        // Generiere eine ausreichend große Stichprobe
        int sampleSize = 100000;
        for (int i = 0; i < sampleSize; i++) {
            int result = w.zieheZahl();
            observedCounts[result - w.getMin()]++;
        }

        // Berechne die erwartete Anzahl für jede Seite
        double expectedCount = sampleSize / (double) (w.getMax() - w.getMin() + 1);

        // Überprüfe, ob jede Abweichung innerhalb des "fairen" Bereichs liegt
        // 5 % um den erwarteten Wert heißt 2,5 % nach oben bzw. unten abweichend
        double maxDeviation = 0.025 * expectedCount;
        for (int count : observedCounts) {
            double deviation = Math.abs(count - expectedCount);
            if (deviation > maxDeviation) {
                return false; // Der Würfel ist nicht fair
            }
        }

        return true; // Der Würfel ist fair
    }
}