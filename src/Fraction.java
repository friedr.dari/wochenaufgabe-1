class Fraction implements Cloneable, Comparable<Fraction> {
    /* ********************************************************************
    /* attributes */
    /* ********************************************************************/
    private int num, denom;   // numerator and denominator of the fraction
  
    /* ********************************************************************
    /* constructors */
    /* ********************************************************************/

    /* create a fraction from two numbers
    /* denominator should be converted to a positive integer without changing
    /* the value of the fraction */
    public Fraction(int numerator, int denominator) throws ZeroDenominatorException {
        setNumDenom(numerator, denominator);
    }
    /* create a fraction from String with a slash e.g. "3/4" */
    public Fraction(String fraction) throws ZeroDenominatorException {
        String[] parts = fraction.split("/");
        int num = Integer.parseInt(parts[0]);
        int denom = Integer.parseInt(parts[1]);
        setNumDenom(num, denom);
    }

    /* ********************************************************************
    /* implementations */
    /* ********************************************************************/

    @Override
    public Fraction clone() {
        try {
            Fraction clone = (Fraction) super.clone();
            clone.num = num;
            clone.denom = denom;
            return clone;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }

    }

    /* compares this fraction to another fraction (negative: smaller, 0: equal, positive: greater) */
    @Override
    public int compareTo(Fraction other) {
        // basic comparing of fractions using cross multiplication
        return num * other.denom - other.num * denom;
    }
  
    /* ********************************************************************
    /* methods */
    /* ********************************************************************/

    /* get and set --------------------------------------------------------- */
    public int getNumerator()   { return num;  }
    public int getDenominator() { return denom;}

    /* type conversion ----------------------------------------------------- */
    /* returns a float representation of the fraction */
    public float toFloat ()     { return ((float) num/ (float) denom); }

    /* returns a String representation like "3/4" of the fraction */
    /* returns a mixed fraction representation */
    public String toString()    {
        // The rationals are supposed to extend the integers cleanly
        if (denom == 1) {
            return Integer.toString(num);
        }

        // If the fraction is improper we have to convert it to a mixed fraction
        if (Math.abs(num) > denom) {
            int whole = num / denom;
            int rest = num % denom;
            return whole + " " + rest + "/" + denom;
        } else {
            return num + "/" + denom;
        }
    }

    /* returns a String representation like "3/4" of the fraction */
    /* returns an improper fraction representation */
    public String toImproperFractionString() {
        // The rationals are supposed to extend the integers cleanly
        if (denom == 1) {
            return Integer.toString(num);
        }

        return num + "/" + denom;
    }

    /* operations ---------------------------------------------------------- */
    /* returns whether or not the fraction equals another fraction */
    /* does not change numerator or denominator of given fractions */
    public boolean equals(Fraction f) {
        // compareTo doesn't mutate so it's fine to use
        return this.compareTo(f) == 0;
    }

    /* reduces the fraction */
    public void reduce() {
        // The greatest common divider is the largest number that divides both
        // so if we divide both by the gcd we cannot divide any further => reduced
        int gcd = gcd(num, denom);
        num /= gcd;
        denom /= gcd;
    }

    /* adds a Fraction b to this Fraction */
    public void add(Fraction b) {
        // a/b + c/d = (a*d + b*c) / (b*d) simple mathematik
        this.num = this.num * b.denom + b.num * this.denom;
        this.denom = this.denom * b.denom;
        this.reduce();
    }

    /* private ------------------------------------------------------------- */

    /* helper method to set the numerator and denominator and to make sure
    /* the denominator is positive */
    private void setNumDenom(int numerator, int denominator) throws ZeroDenominatorException {
        if (denominator == 0) {
            throw new ZeroDenominatorException();
        }

        num   = numerator;
        denom = denominator;

        if (denom < 0) {
            num   = -num;
            denom = -denom;
        }
    }

    /* returns the greatest common divider of a and b*/
    private static int gcd(int a, int b) {
        // Finding the greatest common divider using Euclidean algorithm
        while (b != 0) {
            int h = a % b;
            a = b;
            b = h;
        }

        // gcd should always be positive
        if (a < 0) a = -a;
        return a;
    }

    /* returns the least common multiple of a and b */
    private static int lcm(int a, int b) {
        return b/gcd(a,b)*a;
    }

    /* the main method is just for testing and should be removed later */
    public static void main(String[] args) throws ZeroDenominatorException {
        Fraction f1 = new Fraction("7/15");
        Fraction f2 = new Fraction(21, 45);
        System.out.println("f1 = " + f1.toString() + " = " + f1.toFloat());
        System.out.println("f2 = " + f2.toString() + " = " + f2.toFloat());

        // Erwartet: f1 = f2
        if (f1.equals(f2)) {
            System.out.println("f1 = f2");
        } else {
            System.out.println("f1 != f2");
        }

        f2.reduce();
        // Erwartet: 7/15
        System.out.println(f2.toString());

        f1 = new Fraction(6, 15);
        f2 = new Fraction(3, 8);
        f1.add(f2);

        // Erwartet: 31/40
        System.out.println(f1.toString());

        // Own tests

        System.out.println(new Fraction(5, 2)); // 2 1/2
        System.out.println(new Fraction(5, 2).toImproperFractionString()); // 5/2
        System.out.println(new Fraction(5, 1)); // 5

        System.out.println(new Fraction("3/-2")); // -1/2
    }
} // end of class Fraction

