public class TooManyDiceException extends Exception {
    public TooManyDiceException() {
        super("Zu viele Würfel im Würfelbecher");
    }
}
