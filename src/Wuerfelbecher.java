import java.util.ArrayList;

public class Wuerfelbecher {
    // Die Kapazität von 10 Würfeln wurde für immer und ewig auf itslearning in digitalen Stein gemeißelt
    public static final int MAX_DICE = 10;
    // Bei maximal 10 Würfeln lohnt weder der Performance- noch ein möglicher Speichergewinn durch das Nutzen von Arrays
    // also benutzen wir eine ArrayList
    private final ArrayList<Wuerfel> dice;

    /**
     * Erstellt einen leeren Würfelbecher.
     */
    public Wuerfelbecher() {
        this(new ArrayList<>());
    }

    /**
     * Erstellt einen Würfelbecher mit den gegebenen Würfeln.
     *
     * @param dice die Würfel, die in den Würfelbecher kommen sollen
     */
    public Wuerfelbecher(ArrayList<Wuerfel> dice) {
        this.dice = dice;
    }

    /**
     * Würfelt mit allen Würfeln im Würfelbecher und gibt die Summe der Augenzahlen aus.
     * Wenn der Würfelbecher leer ist, wird keine Exception geworfen, sondern eine Augenzahl von 0 zurückgegeben.
     */
    public void rollDice() {
        // Es würde wahrscheinlich mehr Sinn ergeben die Summe zurückzugeben, aber das ist nicht die Aufgabe

        // Gehe über alle Würfel aus dem Becher, ziehe für jeden eine Zahl und füge sie zur Summe hinzu
        int sum = 0;
        for (Wuerfel die : dice) {
            sum += die.zieheZahl();
        }
        System.out.println("Die Würfel sind gefallen");
        System.out.println("🎲 Das Würfelergebnis ist " + sum);
    }

    /**
     * Fügt einen Würfel zum Würfelbecher hinzu.
     * Wenn der Würfelbecher bereits voll ist (maximal 10 Würfel), wird eine {@link TooManyDiceException} geworfen.
     *
     * @param die der Würfel, der hinzugefügt werden soll
     * @throws TooManyDiceException wenn der Würfelbecher bereits voll ist
     */
    public void addDie(Wuerfel die) throws TooManyDiceException {
        // Es passen höchsten 10 Würfel in den Becher
        if (dice.size() >= MAX_DICE) {
            throw new TooManyDiceException();
        }
        dice.add(die);
    }

    /**
     * Entfernt einen Würfel aus dem Würfelbecher.
     * Wenn der Index außerhalb des gültigen Bereichs liegt, wird eine {@link IndexOutOfBoundsException} geworfen.
     *
     * @param index der Index des Würfels, der entfernt werden soll
     * @return der entfernte Würfel
     * @throws IndexOutOfBoundsException wenn der Index außerhalb des gültigen Bereichs liegt
     */
    public Wuerfel removeDie(int index) throws IndexOutOfBoundsException {
        return dice.remove(index);
    }

    /**
     * Entfernt alle Würfel aus dem Würfelbecher.
     */
    public void clearDice() {
        dice.clear();
    }
}
