public class ZeroDenominatorException extends Exception {
    public ZeroDenominatorException() {
        super("Der Zähler eines Bruches darf nicht 0 sein");
    }
}
