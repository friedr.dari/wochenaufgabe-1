import java.security.SecureRandom;

class Wuerfel {

    /* ********************************************************************
    /* attributes */
    /* ********************************************************************/
    private final int min, max;
    private final SecureRandom rng;

    /* ********************************************************************
    /* constructors */
    /* ********************************************************************/

    /**
     * Erstellt einen Standard sechsseitigen Würfel.
     */
    public Wuerfel() {
         this(1, 6);
     }

    /**
     * Erstellt einen Würfel mit den angegebenen Grenzen.

     * @param min die kleinste Zahl, die der Würfel würfeln kann (inklusive)
     * @param max die größte Zahl, die der Würfel würfeln kann (inklusive)
     */
    public Wuerfel(int min, int max) {
        // Wir verwenden das kryptografisch sichere SecureRandom, um dafür zu sorgen, dass auch ja nicht geschummelt wird
        // und da es noch zufälligere, "fairere" Ergebnisse liefert

        // Wenn min = max haben wir einen einseitigen Würfel, was erlaubt ist
        if (min > max) {
            throw new IllegalArgumentException("min darf nicht größer als max sein.");
        }

        this.rng = new SecureRandom();
        this.min = min;
        this.max = max;
    }

    /* ********************************************************************
    /* methods */
    /* ********************************************************************/
    /**
     * Zieht eine zufällige Zahl zwischen min und max.
     *
     * @return die gezogene Zahl
     */
    public int zieheZahl() {
        // Eine zufällige ganze Zahl von min (inklusive min) bis max + 1 (da nach obenhin offen) also auf [min, max]
        return rng.nextInt(min, max + 1);
    }

    /* getters --------------------------------------------------------- */
    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }
}

