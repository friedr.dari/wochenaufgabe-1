# Wochenaufgabe 1

Achtung: Meine Verwendung von englischer sowie deutscher Sprache im Code ist leider genauso inkonsistent wie in den vorgegebenen Dateien.
Ich habe versucht den Code bzw. die Identifier in Englisch zu halten und den Code dann mit deutschen Kommentaren zu versehen, musste mich aber
in der [Fraction Klasse](src/Fraction.java) den vorhandenen Kommentaren beugen.

## [Würfeltest](src/Wuerfeltester.java) Methode

Um die "fairness" des eines Würfels statistisch aussagekräftig zu prüfen, müsste wahrscheinlich etwas wie ein
Chi-Quadrat-Anpassungstest durchgeführt werden, um zu testen, ob der Würfel eine diskrete Gleichverteilung aufweist.
Allerdings ist die Berechnung des kritischen Wertes anhand von erwünschtem Signifikanzniveau und Anzahl an Freiheitsgraden nicht ohne weiteres ohne die Verwendung externer Bibliotheken möglich.

Deshalb wird hier eine andere Methode zur Bewertung der "fairness" verwendet, welche zumindest das Gefühl eines "fairen" Würfel
beispielsweise beim Spielen mit Freunden widerspiegeln sollte, obwohl sie statistisch nicht aussagekräftig ist.

Als "fair" gilt hier ein Würfel, wenn nicht eine Seite signifikant wahrscheinlicher vorkommt.
Das ist der Fall, wenn für jede Seite bei einem ausreichend großen Umfang der Stichprobe für jede Seite die beobachtete Anzahl nicht außerhalb
eines Bereichs von 5 % um die erwartete Anzahl liegt.

Verfahren:
1. Nullhypothese aufstellen: Von einem gegebenen Würfel wird behauptet, dass es sich um einen "fairen" Würfel handelt.
2. Aussuchen eines Stichprobenumfangs: Dies ist bei unserem digitalen Würfel nicht schwer, da wir deutlich mehr als notwendig an Stichproben generieren können.
3. Durchführung einer einfachen Zufallsstichprobe
4. Berechnen der prozentualen Abweichung für jede potenzielle Realisation
5. Liegt jede Abweichung für jede Seite unter den 2,5 % maximaler Abweichung, gilt der Würfel als "fair"